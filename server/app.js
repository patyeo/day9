/*
* this app.js is server side
* Express middleware
*/

var express = require("express");
var bodyParser = require("body-parser");
var moment = require ('moment');
moment().format();

console.log(__dirname);
const NODE_PORT = process.env.PORT || 4000;
console.log(NODE_PORT);

var app = express();

//this is to tell server to convert data key in client side to json file, so that it can read.
// and limit the size to 50mb to prevent hacking
app.use(bodyParser.urlencoded({limit:'50mb', extended:true}));
app.use(bodyParser.json({limit:'50mb'}));

//var regForm = require("./reg.json");
//console.log(regForm);
app.use(express.static(__dirname + "/../client/"));

app.get("/api/emptyForm",(req,res)=>{
    console.log("get emptyForm");
    res.status(200).json(regForm);
});
// '/api/popquizes' is a endpoint, a url so need to include '/' while server/app.js is an executable file
app.post("/api/submitForm",(req,res)=>{
    var registeredUser = req.body;
    // the server side's req.body will match the client side's regCtrlself.user
    // the client side's result will match the server side's registeredUser
    console.log(req.body);
    console.log("email> " + registeredUser.email);
    console.log("password> " + registeredUser.password);
    console.log("confirmpassword >" + registeredUser.confirmpassword);
    console.log("fullName >" + registeredUser.fullName);
    console.log("gender >" + registeredUser.gender);
    var newDob = moment (registeredUser.dob);
    console.log(newDob.toLocaleString());
    console.log("address >" + registeredUser.address);
    console.log("nationality >" + registeredUser.nationality);
    console.log("contactNumber >" + registeredUser.contactNumber);
    res.status(200).json(registeredUser);
// var newDob = moment (registeredUsser.dob)
// console.log (newDOB.toLocaleString())
// the above is to adjust the date is the date reflected is not the date chosen due to locale date difference.
// remember to npm install moment --save to install moment into the folder first


    // convert body choice to integer.
//    var choiceInt = parseInt(req.body.choice);
//    var correctAnswer = parseInt(popQuiz.correctAnswer);

});
// created another end point for the answers to the quiz to be posted to

app.use((req,res)=>{
    var y = 6;
    try{
        console.log("Y : " + y);
        res.send(`<h1> Oopps wrong please ${y}</h1>`);
    }catch(error){
        console.log(error);
        res.status(500).send("ERROR");
    } 
});



app.listen(NODE_PORT, ()=>{
    console.log(`Web App started at ${NODE_PORT}`);
}); 