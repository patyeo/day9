(function () {
    "use strict";
    angular.module("RegApp")
        .controller("AnimateCtrl", AnimateCtrl);

    AnimateCtrl.$inject = [];

    
    function AnimateCtrl(){
        var self = this;
        self.boxClass = true;
        self.circleClass = false;
        self.triangleClass = false;

    }
})();