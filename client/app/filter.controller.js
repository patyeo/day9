(function () {
    "use strict";
    angular.module("RegApp")
        .controller("FilterCtrl", FilterCtrl);

    FilterCtrl.$inject = ['$filter'];

    function FilterCtrl(){
        var self = this;
        self.search = "";
        self.propertyName = "id";
        self.reverse = false;
        self.today = new Date();

        self.items = [
            { id: 1, name: "A", age: 20 },
            { id: 5, name: "E", age: 20 },
            { id: 6, name: "F", age: 20 },
            { id: 2, name: "B", age: 20 },
            { id: 3, name: "C", age: 23 },
            { id: 4, name: "D", age: 22 },
            { id: 7, name: "G", age: 21 },
            { id: 8, name: "H", age: 21 },
            { id: 9, name: "I", age: 20 },
            { id: 10, name: "A", age: 20 }
        ]

        self.sortBy = function(propertyName){
            console.log(!self.reverse);
            console.log(self.propertyName === propertyName);
            if(self.propertyName === propertyName){
                self.reverse = !self.reverse;
            }else{
                self.reverse = false;
            }
            //self.reverse = (self.propertyName === propertyName) ? !self.reverse : false;
            // this is a shorter version of the if, else function
            console.log(self.reverse);
            self.propertyName = propertyName;
        }
    }

})();