(function () {
    "use strict";
    angular
        .module("RegApp")
        .controller("RegCtrl", RegCtrl);
        .controller("RegisteredCtrl", RegisteredCtrl);

// custom service '' and angular servce can be '' or ""
    RegCtrl.$inject = ['RegServiceAPI', "$log"];
    RegisteredCtrl.$inject = ['RegServiceAPI', "$log"];
    
    function RegisteredCtrl(RegServiceAPI, $log){
        var self = this;
        self.fullname = RegServiceAPI.fullname;
    }

    function RegCtrl(RegServiceAPI, $log) {
        var regCtrlself = this;
        
        regCtrlself.onSubmit = onSubmit;
        regCtrlself.initForm = initForm;
        regCtrlself.onlyFemale = onlyFemale;
        regCtrlself.user ={

        }
        regCtrlself.nationalities =[
            {name: "Please select", value: "0"},
            {name: "Singaporean", value: "1"},
            {name: "Malaysian", value: "2"},
            {name: "Indonesian", value: "3"},
            {name: "Thai", value: "4"},
            {name: "Others", value: "5"}
        ];
    
        function initForm() {
            regCtrlself.user.selectNationality="0";
            // this regCtrlself.... is to tell the function when they init form to auto show value 0
            regCtrlself.user.gender = "F";
            // this is to let the form be loaded with Female being checked and the span will not be activated. what is written in html is only at user interface.
        }
        function onReset(){
            regCtrlself.user = Object.assign({}, regCtrlself.user);
            regCtrlself.registrationform.$setPristine();
            regCtrlself.registrationform.$setUntouched();
        }

        function onSubmit(){
            RegServiceAPI.register(regCtrlself.user)
                .then((result)=>{
                    regCtrlself.user = result.data;
                    console.log(RegServiceAPI)
                }).catch((error)=>{
                    console.log(error);
                })            
        }

        function onlyFemale(){
            console.log("only female");
            return regCtrlself.user.gender=="F";
        }

    }
    regCtrlself.initForm();
    }

})();